// Import thư viện express
const express = require("express"); // import express from "express";

// Import thư viện mongoose
const mongoose = require("mongoose")

// Khởi tạo 1 app express
const app = express();

// Khai báo cổng sẽ chạy
const port = 8000;

// kết nối mongodb
mongoose.connect("mongodb://127.0.0.1:27017/CRUD_Course")
        .then(() =>  console.log('Connected to mongodb successfully!'))
        .catch((err) => console.error(err));

// Khai báo các model
const reviewModel = require("./app/models/review.model")
const courseModel = require("./app/models/course.model")

// Khai báo router
const courseRouter = require("./app/routes/course.router");
const reviewRouter = require("./app/routes/review.router");

// Khai báo để ứng dụng đọc được request body json
app.use(express.json());

/**** Task 506.10 Application Middleware ******/
const methodMiddleWare =  (req, res, next) => {
    console.log(req.method);

    next();
}

app.use((req, res, next) => {
    console.log("Current time:", new Date());
    next();
})
/*
app.use((req, res, next) => {
    console.log("Current time:", new Date());
    next();
}, methodMiddleWare)
*/
app.use((req, res, next) => {
    console.log("Request method:", req.method);
    next();
})

app.get("/", (req, res) => {
    let today = new Date();

    res.json({
        message: `Xin chào, hôm nay là ngày ${today.getDate()} tháng ${today.getMonth() + 1} năm ${today.getFullYear()}`
    })
})

app.get("/get-method", (req, res) => {
    res.json({
        message: "Get method"
    })
})

app.post("/post-method", (req, res) => {
    res.json({
        message: "Post method"
    })
})

app.put("/put-method", (req, res) => {
    res.json({
        message: "Put method"
    })
})

app.delete("/delete-method", (req, res) => {
    res.json({
        message: "Delete method"
    })
})

//Request params
// Được gắn vào url
app.get("/request-params/:param1/:param2", (req, res) => {
    const param1 = req.params.param1;
    const param2 = req.params.param2;

    // Validate kiểu dữ liệu

    res.json({
        param1: param1,
        param2: param2
    })
})

// Request query
// Request query không là 1 thành phần trong khai báo URL
app.get("/request-query", (req, res) => {
    const query = req.query;

    // Với những bài toán khai thác request query thì chúng ta cần validate dạng có|không

    res.json({
        message: "Request query",
        query: query
    })
})

app.post("/request-body", (req, res) => {
    const body = req.body;

    res.json({
        message: "Request body",
        body: body
    })
})

app.use("/api/v1", courseRouter);
app.use("/api/v1", reviewRouter);

app.listen(port, () => {
    console.log("App listening on port:", port);
})