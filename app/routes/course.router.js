const express = require("express");

const router = express.Router();

const courseMiddleware = require("../middlewares/course.middleware");

router.get("/courses", courseMiddleware.getAllCourseMiddleware, (req, res) => {
    return res.send({
        message: "Get All Course"
    })
})

router.post("/courses", courseMiddleware.createCourseMiddleware, (req, res) => {
    return res.send({
        message: "Create Course"
    })
})

router.get("/courses/:courseid", courseMiddleware.getCourseByIDMiddleware, (req, res) => {
    const courseid = req.params.courseid;

    return res.send({
        message: "Get Course ID = " + courseid
    })
})

router.put("/courses/:courseid", courseMiddleware.updateCourseMiddleware, (req, res) => {
    const courseid = req.params.courseid;

    return res.send({
        message: "Update Course ID = " + courseid
    })
})

router.delete("/courses/:courseid", courseMiddleware.deleteCourseMiddleware, (req, res) => {
    const courseid = req.params.courseid;

    return res.send({
        message: "Delete Course ID = " + courseid
    })
})

module.exports = router; // Tương tự export router

