const createReviewMiddleware = (req, res, next) => {
    console.log("Create Review Middleware");
    next();
}

const getAllReviewMiddleware = (req, res, next) => {
    console.log("Get All Review Middleware");
    next();
}

const getReviewByIDMiddleware = (req, res, next) => {
    console.log("Get Detail Review Middleware");
    next();
}

const updateReviewMiddleware = (req, res, next) => {
    console.log("Update Review Middleware");
    next();
}

const deleteReviewMiddleware = (req, res, next) => {
    console.log("Delete Review Middleware");
    next();
}

module.exports = {
    createReviewMiddleware,
    getAllReviewMiddleware,
    getReviewByIDMiddleware,
    updateReviewMiddleware,
    deleteReviewMiddleware
}
